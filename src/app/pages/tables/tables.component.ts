import { Component, OnInit } from '@angular/core';
import { TradesService } from '../../trades.service';


@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {

  public tradeData = {};
  public ticker = ["C", "GOOGL", "NFLX", "TSLA", "FB"];
  public numDays = 1;
  public quantity = {};

  constructor(private tradesService: TradesService) { }

  ngOnInit() {
    this.ticker.forEach(element => {
      this.getMarketValue(element, this.numDays);
    });
  }

  getMarketValue(ticker: string, numDays: number) {
    this.tradesService.getMarketValues(ticker, numDays).subscribe(
      response => {
        console.log("retrieved price data:");
        // this.tradeData.push({ ticker: response.price_data[0][1] });
        this.tradeData[ticker] = response.price_data[0][1];
        console.log(this.tradeData);
      },
      error => {
        console.log("Error reading  price data:");
        console.log(error);
      }
    )
  }

  objectKeys(obj) {
    return Object.keys(obj);
  }

  buyStock(item: any, quantity: any) {
    console.log("Item Buy: " + item.key + " " + quantity);
    let tradeRequest = JSON.stringify({
      "name": item.key,
      "quantity": quantity,
      "price": item.value,
      "tradeType": "BUY"
    });
    this.tradesService.save(tradeRequest).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log("Error reading  price data:");
        console.log(error);
      }
    );
    alert("You just bought " + item.key + " stock with quantity " + quantity + " and total price $" + (item.value * quantity));
  }

  sellStock(item: any, quantity: any) {
    console.log("Item Sell: " + item.key + " " + quantity);
    let tradeRequest = JSON.stringify({
      "name": item.key,
      "quantity": quantity,
      "price": item.value,
      "tradeType": "SELL"
    });
    this.tradesService.save(tradeRequest).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log("Error reading  price data:");
        console.log(error);
      }
    );
    alert("You just sold " + item.key + " stock with quantity " + quantity + " and total price $" + (item.value * quantity));
  }

}
