import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';

import { TradesService } from '../../trades.service';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "../../variables/charts";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  public tradeData: any;

  public monthlyData = [];
  public weeklyData = [];
  public tickerValue = "C";

  constructor(private tradesService: TradesService) { }

  ngOnInit() {

    this.getTrades();
    this.getTradesForSpecifiedTicker(this.tickerValue);

    this.datasets = [
      this.monthlyData, this.weeklyData
    ];

    this.data = this.monthlyData;
    chartExample1.data.datasets[0].data = this.data;

    var chartOrders = document.getElementById('chart-orders');

    parseOptions(Chart, chartOptions());


    var ordersChart = new Chart(chartOrders, {
      type: 'bar',
      options: chartExample2.options,
      data: chartExample2.data
    });

    var chartSales = document.getElementById('chart-sales');

    this.salesChart = new Chart(chartSales, {
      type: 'line',
      options: chartExample1.options,
      data: chartExample1.data
    });
  }


  public updateOptions() {
    this.salesChart.data.datasets[0].data = this.data;
    this.salesChart.update();
  }

  public getTrades() {
    this.tradesService.getTrades().subscribe(
      response => {
        console.log("retrieved price data:")
        console.log(response);
        this.tradeData = response;
      },
      error => {
        console.log("Error reading  price data:");
        console.log(error);
      }
    )
    console.log(this.tradeData);
  }

  public getTradesForSpecifiedTicker(ticker) {
    this.tradesService.getMarketValues(ticker, 30).subscribe(
      response => {
        console.log("retrieved price data:")
        console.log(response);
        console.log(response.price_data);
        response.price_data.forEach(element => {
          this.monthlyData.push(element[1])
        });
      },
      error => {
        console.log("Error reading  price data:");
        console.log(error);
      }
    )

    console.log(this.monthlyData);

    this.tradesService.getMarketValues(ticker, 7).subscribe(
      response => {
        console.log("retrieved price data:")
        console.log(response);
        response.price_data.forEach(element => {
          this.weeklyData.push(element[1])
        });
      },
      error => {
        console.log("Error reading  price data:");
        console.log(error);
      }
    )
  }

}
