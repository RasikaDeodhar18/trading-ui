import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TradesService {

  constructor(private httpClient: HttpClient) { }

  public getTrades(): Observable<any> {
    return this.httpClient.get(
      environment.tradeUrl + "v1/trade"
    )
  }

  public getMarketValues(ticker: string, numDays: number = 1): Observable<any> {
    return this.httpClient.get(
      environment.priceUrl + "?ticker=" + ticker + "&num_days=" + numDays
    )
  }

  public save(stock: any): Observable<any> {
    return this.httpClient.post<any>(
      environment.tradeUrl + "v1/trade",
      stock,
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }
}
